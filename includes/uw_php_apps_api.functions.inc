<?php
/**
 * @file
 *
 * API functions
 */

function _uw_php_apps_api_request($options) {
  $response = FALSE;

  if (!empty($options)) {
    $ch = curl_init();

    if ($ch && curl_setopt_array($ch, $options)) {
      $response_raw = curl_exec($ch);
      $response_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      if ($response_status === 200) {
        $response = json_decode($response_raw, TRUE);
        if (is_array($response)) {
          $response = array_change_key_case($response);
        }
      }
    }
  }

  return $response;
}


function _uw_php_apps_api_token_options($module) {
  $options = [];
  $api_fields = FALSE;
  $api_token_url = FALSE;

  $api_conf = variable_get($module);

  if ($api_conf) {
    if (isset($api_conf['connection'])) {
      $api_fields = $api_conf['connection'];
    }

    if (isset($api_conf['api_token_url'])) {
      $api_token_url = $api_conf['api_token_url'];
    }
  }

  if ($api_token_url && $api_fields) {
    $options = [
      CURLOPT_POST => TRUE,
      CURLOPT_POSTFIELDS => $api_fields,
      CURLOPT_URL => $api_token_url,
      CURLOPT_RETURNTRANSFER => TRUE,
    ];
  }

  return $options;
}

function _uw_php_apps_api_userid_options($uwuserid, $api_token, $module='uw_php_apps_api') {
  $options = [];
  $api_user_url = FALSE;
  $api_conf = variable_get($module);

  if ($api_conf && isset($api_conf['api_user_url'])) {
    $api_user_url = $api_conf['api_user_url'];
  }

  if ($api_user_url && $uwuserid && $api_token) {
    $api_user_specific_url = format_string($api_user_url, ['@uwuserid' => $uwuserid]);

    $auth = "Authorization: Bearer $api_token";

    $options = [
      CURLOPT_HTTPHEADER => array($auth),
      CURLOPT_POST => FALSE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_URL => $api_user_specific_url,
    ];
  }

  return $options;
}

function uw_php_apps_api_user_data($uwuserid, $module='uw_php_apps_api', $sanitize = FALSE) {
  $user_data = [];

  if ($uwuserid) {
    $auth_options = _uw_php_apps_api_token_options($module);
    $auth = _uw_php_apps_api_request($auth_options);
    $token = isset($auth['access_token']) ? $auth['access_token'] : FALSE;

    if ($token) {
      $user_options = _uw_php_apps_api_userid_options($uwuserid, $token, $module);
      $user_data = _uw_php_apps_api_request($user_options);
    }
  }

  return $sanitize ? _uw_php_apps_api_sanitize($user_data) : $user_data;
}

/**
 *  Removes additional prefixes from fields.
 *
 *  Making sure all array keys are lower case, and updating hr and student number.
 */
function _uw_php_apps_api_sanitize($data) {
  $sanitized = $data;

  $patterns = array(
    'hrids' => "/^HR(\d{5,})/i",
    'studentid' => "/^S(\d{8})/i",
  );

  foreach ($patterns as $field => $pattern) {
    $validate = isset($sanitized[$field]);

    if ($validate) {
      $sanitized[$field] = _uw_php_apps_api_sanitize_validate_field($sanitized[$field], $pattern);
    }
  }

  return $sanitized;
}

function _uw_php_apps_api_sanitize_validate_field($input_value, $pattern = FALSE) {
  $value = FALSE;
  $multivalue = explode(',', $input_value);

  if (is_array($multivalue)) {
    if (!empty($multivalue) && isset($multivalue[0])) {
      $value = $multivalue[0];
    }
  }

  if ($pattern && $value) {
    $matches = array();

    if (preg_match_all($pattern, $value, $matches)) {
      if ($matches && !empty($matches) && isset($matches[1][0])) {
        $value = $matches[1][0];
      }
    }
  }

  return $value;
}
