# UW PHP API module #

This project is UW API implemented as Drupal module. Intended usage is for migrated legacy PHP applications that require access to student data such as names, student number etc.

## Requirements ##

No module dependencies. But here is configuration dependency, following snippet needs to be added to active settings.php file:

~~~
$conf['uw_php_apps_api']['connection'] = array(
  'scope' => '',
  'grant_type' => 'client_credentials',
  'client_id' => '',
  'client_secret' => '',
);

$conf['uw_php_apps_api']['api_token_url'] = '';
$conf['uw_php_apps_api']['api_user_url'] = '';
~~~

Use @uwuserid as placeholder for user id that needs to be placed in url. Example:
`https://myapi.data.com/@uwuserid/extended`

If configuration setting is not found, module will not work. 


## How it all works ##

For each user data request there will be two API calls. First one will get access token, and second call will get requested user details. 
  
## Intended use

This module is created in order for UW Endowment Application as well as UW Tuition Benefit Application use API when working with student details. 
